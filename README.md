# Really Bad Haiku

This application will generate a bad haiku from a pre-existing list of words.


The default is to follow the 5-7-5 syllable structure of a typical haiku...

- [Access the application here](https://bad-haiku.herokuapp.com/)

Though you can also add a comma seperated query param if you'd like an alternative structure.

- [Here's an example of that here](https://bad-haiku.herokuapp.com/?syllables=3,9,2)

There's also an endpoint for returning the data in list form instead of markup to hook into API's like the proposed slack integration from the prompt.

- [Data endpoint](https://bad-haiku.herokuapp.com/data?syllables=3,9,2)


## Known Issues in "Beta"

This is only hosted at the git level in heroku for lack of time and as to not go down the pipeline/containerization wormhole since it's better to deploy to a "actual" managed service if your going to invest for real (containers, pipelines, real managed services, on file credit cards...)

Current setup wouldn't scale to larger data sets well since necessitates lots of looping to setup matches and evaluates across both a large dict and a library. This would be easily mitigated by caching or db scaling (or you know ... just having any of this at a db level at all).

# Prompt

You and a coworker of yours want to make a haiku generator. It’s not expected to be any good at all; it’s more like a joke for when someone’s bored and wants to trigger something on Slack. 

Your coworker has provided a .txt file of two hundred unique words in English that he wants to use (`words.txt`). Your job is to take works from that set, parse them to understand how many syllables they have, and assemble a haiku (3 lines, with 5, 7, and 5 syllables, respectively). Besides the haiku format, you want to avoid using duplicate words.

On counting syllables, we know English is weird and can have a lot of exceptions. Don't spend hours on being exhaustive.

You may implement this in any language of your choosing, but we'd prefer to see your Python skills. If you have time to add whatever you'd need to deem this "production-ready", we'd love to see that as well!

Fork this repository and share a link to your completed work when you're done.
