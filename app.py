import os
import random
from collections import defaultdict
import cmudict

from flask import Flask, request
from textstat import textstat

d = cmudict.dict()

def parse_words_to_list(path):
    with open(path) as f:
        words = [word.strip() for word in f.readlines()]
    return words

def count_syllables(word):
    """Use cmudict to find words else try with textstat"""
    dictresult = [len(list(y for y in x if y[-1].isdigit())) for x in d[word.lower()]]
    if len(dictresult) > 0 and dictresult[0] > 0:
        return dictresult[0]
    else:
        return textstat.syllable_count(word)

def generate_words_dict():
    """Creates dict where number of syllables is the key to lookup words."""
    dicts = defaultdict(list)
    words = parse_words_to_list('words.txt')
    for word in words:
        syllables_count = count_syllables(word)
        dicts[syllables_count].append(word)
    return dicts


def generate_line_schema(required_syllables, available_syllables):
    """Creates set of syllables required to form haiku from what is available."""
    if required_syllables == 1:
        return [1]
    line_schema = []
    while required_syllables > 0:
        max_syllables = min(required_syllables, max(available_syllables))
        random_smaller_int = random.randint(1, max_syllables)
        line_schema.append(random_smaller_int)
        required_syllables -= random_smaller_int
    return line_schema


def find_word_with_key(syllable_list):
    """Matches words with keys existing in the haiku dictionary."""
    random_index = random.randint(0, len(syllable_list) - 1)
    haiku_word = syllable_list[random_index]
    return haiku_word


def generate_haiku(syllables_per_line=[5, 7, 5]):
    """Generates haiku in list format with optional args to create other haiku-like poems."""
    words_dict = generate_words_dict()
    haiku = []
    for required_syllables in syllables_per_line:
        haiku_sentence = ''
        current_line_schema = generate_line_schema(required_syllables, list(words_dict.keys()))
        for syllable_key in current_line_schema:
            haiku_sentence += find_word_with_key(words_dict[syllable_key]) + " "
        haiku_sentence_capitalized = haiku_sentence[0].upper() + haiku_sentence[1:len(haiku_sentence) - 1]
        haiku.append(haiku_sentence_capitalized)
    return haiku


# Flask app to display haiku
app = Flask(__name__)

@app.route('/')
def home():
    syllables_per_line = [5, 7, 5]
    if 'syllables' in request.args:
        syllables_per_line = [int(s) for s in request.args['syllables'].split(',')]
    haiku = generate_haiku(syllables_per_line)
    haiku_html = ''.join([f'<div>{line}</div>' for line in haiku])
    return f'<div>{haiku_html}</div>'


@app.route('/data')
def data():
    syllables_per_line = [5, 7, 5]
    if 'syllables' in request.args:
        syllables_per_line = [int(s) for s in request.args['syllables'].split(',')]
    haiku = generate_haiku(syllables_per_line)
    return '\n'.join(haiku)

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)

